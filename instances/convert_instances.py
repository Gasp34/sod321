# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 12:04:21 2020

@author: Gaspard
"""

name = "aerodrome_80_1"
f = open(f"{name}.txt","r")
l = f.readlines()

n = int(l[0])
deb = int(l[1])
fin = int(l[2])
amin = int(l[3])
m = int(l[4])
region = [int(i) for i in l[6].split(" ")[:-1]]
R = int(l[8])

coord = []
for i in range(10,10+n):
    x,y = l[i].split(" ")
    x,y = int(x), int(y)
    coord.append([x,y])
 
f.close()    

f = open(f"{name}.dat","w")
f.write(f"param n := {n};\n")
f.write(f"param m := {m};\n")
f.write(f"param deb := {deb};\n")
f.write(f"param fin := {fin};\n")
f.write(f"param amin := {amin};\n")
f.write("param region := \n")
for i in range(len(region)):
    f.write(f"{i+1} {region[i]}\n")
f.write(";\n")
f.write(f"param R := {R};\n")
f.write("param coord := \n")
for i in range(len(coord)):
    for j in range(2):
        f.write(f"{i+1} {j+1} {coord[i][j]}\n")
f.write(";")
f.close()
#modele avec un nombre polynomial de contraintes

#donnees

param n; #nombre d'aerorodrome
set N := {1..n};
param m; #nombre de region
param deb; #aerodrome de depart
param fin; #aerodrome d'arrivee
param amin; #nombre minimal d'aerodromes a parcourir
param region{i in N}; #region de chaque aerodrome
param R; #rayon maximal qu'un avion peut parcourir sans se poser
param coord{i in N, 1..2}; #coordonnees de chaque aerodrome
set K := N diff {deb,fin};
set Rk{k in 0..m} = {i in N : region[i] = k};
param d{i in N, j in N} := round(sqrt((coord[i,1]-coord[j,1])^2 + (coord[i,2]-coord[j,2])^2));
set A := {i in N, j in N diff {i}: d[i,j] <= R};

#variables

var x{(i,j) in A} binary;
var v{i in N} binary;
var u{i in N} >= 0;

#objectif

minimize f: sum{(i,j) in A} d[i,j]*x[i,j];

#contraintes (se referer au rapport pour leur signification)

subject to
c1 : sum{i in N} v[i] >= amin;
c2 {k in 1..m} : sum{i in Rk[k]} v[i] >= 1; #visite toutes les regions
c3 : sum{j in N : (j,deb) in A} x[j,deb] = 0;
c4 : sum{i in N : (i,fin) in A} x[i,fin] = 1;
c5 : sum{j in N : (deb,j) in A} x[deb,j] = 1;
c6 : sum{j in N : (fin,j) in A} x[fin,j] = 0;
c7 {(i,j) in A} : u[j] >= u[i] + 1 - n*(1-x[i,j]); #elimine les sous-tours
c8 {i in N diff {fin}} : v[i] = sum{j in N, (i,j) in A} x[i,j]; #determine v
c9 : v[fin] = 1;
c10 {i in K} : sum{j in N : (i,j) in A} x[i,j] - sum{j in N : (j,i) in A} x[j,i] = 0; #connexite

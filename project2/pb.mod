#modele avec un nombre exponentiel de contraintes

# ----------------------------------------
# le problème Maître
# ----------------------------------------

#donnees

param n; #nombre d'aérorodrome
set N := {1..n};
param m; #nombre de region
param deb;
param fin;
param amin;
param region{i in N};
param R;
param coord{i in N, 1..2};
set K := N diff {deb,fin};
set Rk{k in 0..m} = {i in N : region[i] = k};
param d{i in N, j in N} := round(sqrt((coord[i,1]-coord[j,1])^2 + (coord[i,2]-coord[j,2])^2));
set A := {i in N, j in N diff {i}: d[i,j] <= R};

param p integer >= 0;
param ay{i in N, j in N, k in 1..p} >= 0;
param az{i in N, k in 1..p} binary;
#variables

var x{(i,j) in A} binary;
var v{i in N} binary;

#objectif et contraintes (se referer au rapport pour leur signification)

minimize f: sum{(i,j) in A} d[i,j]*x[i,j];

subject to
c1 : sum{i in N} v[i] >= amin;
c2 {k in 1..m} : sum{i in Rk[k]} v[i] >= 1; #visite toutes les regions
c3 : sum{j in N : (j,deb) in A} x[j,deb] = 0;
c4 : sum{i in N : (i,fin) in A} x[i,fin] = 1;
c5 : sum{j in N : (deb,j) in A} x[deb,j] = 1;
c6 : sum{j in N : (fin,j) in A} x[fin,j] = 0;
c8 {i in N diff {fin}} : v[i] = sum{j in N, (i,j) in A} x[i,j]; #determine v
c9 : v[fin] = 1;
c10 {i in K} : sum{j in N : (i,j) in A} x[i,j] - sum{j in N : (j,i) in A} x[j,i] = 0; #connexite
c11 {k in 1..p} : sum{(i,j) in A} x[i,j]*ay[i,j,k] <= sum{i in N} az[i,k] - 1;

# ----------------------------------------
# le sous-problème
# ----------------------------------------

var y{(i,j) in A} >= 0;
var z{i in N} binary;
param coeff{(i,j) in A};

maximize st: sum{(i,j) in A} coeff[i,j]*y[i,j] - sum{i in N} z[i] + 1;

subject to
def1{(i,j) in A : j <= i} : y[j,i] = y[i,j];
def2{(i,j) in A : j <= i} : y[i,j] <= z[i];
def3{(i,j) in A : j <= i} : y[i,j] <= z[j];
card1 : sum{i in N} z[i] >= 2;
card2 : sum{i in N} z[i] <= n-2;

